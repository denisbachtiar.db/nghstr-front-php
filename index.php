<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="/assets/favicon.ico" rel="shortcut icon">

    <title>Web Hosting Indonesia Unlimited & Terbaik - Niagahoster</title>
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/costum.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css">
</head>

<body>
    <div class="navbar-full">
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar3">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="/assets/images/niagahosterlogo.png" alt="Niagahoster Logo" />
                    </a>
                </div>
                <div id="navbar3" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right navbar-bg">
                        <li>
                            <a href="#">Hosting</a>
                        </li>
                        <li>
                            <a href="#">Domain</a>
                        </li>
                        <li>
                            <a href="#">Server</a>
                        </li>
                        <li>
                            <a href="#">Website</a>
                        </li>
                        <li>
                            <a href="#">Afiliasi</a>
                        </li>
                        <li>
                            <a href="#">Promo</a>
                        </li>
                        <li>
                            <a href="#">Pembayaran</a>
                        </li>
                        <li>
                            <a href="#">Review</a>
                        </li>
                        <li>
                            <a href="#">Kontak</a>
                        </li>
                        <li>
                            <a href="#">Blog</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <section class="headline">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <h2>PHP Hosting</h2>
                    <h3>
                        Cepat, handal, penuh dengan
                        <br />modul PHP yang Anda butuhkan
                    </h3>

                    <div>
                        <table class="table-responsive">
                            <tbody>
                                <tr>
                                    <td class="checklist">
                                        <i class="fas fa-check-circle"></i>
                                    </td>
                                    <td>Solusi PHP untuk performa query yang lebih cepat.</td>
                                </tr>
                                <tr>
                                    <td class="checklist">
                                        <i class="fas fa-check-circle"></i>
                                    </td>
                                    <td>Konsumsi Memori yang lebih rendah.</td>
                                </tr>
                                <tr>
                                    <td class="checklist">
                                        <i class="fas fa-check-circle"></i>
                                    </td>
                                    <td>Support PHP 5.3, PHP 5.4, PHP 5.5, PHP 5.6, PHP 7</td>
                                </tr>
                                <tr>
                                    <td class="checklist">
                                        <i class="fas fa-check-circle"></i>
                                    </td>
                                    <td>Fitur enkripsi IonCube dan Zend Guard Loaders</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 slideanim">
                    <img src="/assets/svg/illustration banner PHP hosting-01.svg" />
                </div>
            </div>
        </div>
    </section>

    <section class="fitur">
        <div class="container">
            <div class="row">
                <div id="fitur" class="container-fluid text-center">
                    <div class="row text-center slideanim">
                        <div class="col-sm-4">
                            <div class="thumbnail">
                                <div class="thumbs">
                                    <img src="/assets/images/zendguard.png" />
                                </div>
                                <span>
                                    <p>PHP Zend Guard Loader</p>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="thumbnail">
                                <img src="/assets/images/composer.png" />
                                <p>PHP Composer</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="thumbnail">
                                <div class="thumbs">
                                    <img src="/assets/images/ioncube.png" />
                                </div>
                                <span>
                                    <p>PHP Ioncube Loader</p>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="harga" id="harga">
        <div class="container">
            <div class="col-sm-6 col-md-12">
                <div class="row text-center slideanim">
                    <h2>
                        <b>Paket Hosting Singapura yang Tepat</b>
                    </h2>
                    <h3>Diskon 40% + Domain dan SSL Gratis untuk Anda</h3>
                    <br />
                    <br />

                    <?php
                    $json = file_get_contents('assets/json/paket.json');
                    $folders = json_decode($json);
                    foreach ($folders as $key => $value) {
                        echo '
                            <div class="col-md-3 col-sm-6">';
                        if ($value->best_seller) {
                            echo '<div class="harga-table side personal">';
                        } else {
                            echo '<div class="harga-table side">';
                        }
                        echo '
                            <div class="harga-header">
                                <div class="harga-nama">
                                    <b>' . $value->title . '</b>
                                </div>
                                <div class="harga-nominal">
                                    <h4>Rp ' . $value->original_price . '</h4>
                                    
                                    ' . $value->price . '
                                </div>
                                <div class="harga-nama user">
                                    <h5>
                                        <b>' . $value->user . '</b> Pengguna Terdaftar
                                    </h5>
                                </div>
                            </div>';

                        if ($value->best_seller) {
                            echo '<div class="harga-ribbon harga-ribbon-top-left">
                                <span>Best Seller!</span>
                              </div>';
                        } else {
                            echo '';
                        }

                        echo '
                            <div class="harga-body">
                                <ul>';
                        foreach ($value->list as $key => $list) {
                            echo '
                                        <li>
                                           ' . $list . '
                                        </li>';
                        }
                        echo '
                                    <li>
                                        <br />
                                        <div class="harga-footer">';
                        if ($value->best_seller) {
                            echo '<a href="#" class="btn btn-main hrf">Pilih Sekarang</a>';
                        } else {
                            echo '<a href="#" class="btn btn-default">Pilih Sekarang</a>';
                        }
                        echo '</div>
                                    </li>
                                </ul>
                            </div>
                            </div>
                            </div> 
                            ';
                    } ?>
                </div>
            </div>
        </div>
    </section>

    <section class="limit-php">
        <div class="container">
            <div class="col-sm-6 col-md-12">
                <div class="row text-center">
                    <h3>Powerfull dengan limit PHP yang lebih Besar</h3>
                    <br />

                    <div class="row">
                        <div class="col-md-4 col-md-offset-2">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td class="checklist">
                                            <i class="fas fa-check-circle"></i>
                                        </td>
                                        <td>max execution time 300s</td>
                                    </tr>

                                    <tr class="active">
                                        <td class="checklist">
                                            <i class="fas fa-check-circle"></i>
                                        </td>
                                        <td>max execution time 300s</td>
                                    </tr>

                                    <tr>
                                        <td class="checklist">
                                            <i class="fas fa-check-circle"></i>
                                        </td>
                                        <td>php memory limit 1024 MB</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-4">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td class="checklist">
                                            <i class="fas fa-check-circle"></i>
                                        </td>
                                        <td>post max size 128 MB</td>
                                    </tr>

                                    <tr class="active">
                                        <td class="checklist">
                                            <i class="fas fa-check-circle"></i>
                                        </td>
                                        <td>Upload max filesize 128 MB</td>
                                    </tr>

                                    <tr>
                                        <td class="checklist">
                                            <i class="fas fa-check-circle"></i>
                                        </td>
                                        <td>max input vars 2500</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <hr />
                </div>
            </div>
        </div>
    </section>

    <section class="support">
        <div class="container-fluid text-center">
            <h3>Semua Paket Hosting Sudah Termasuk</h3>
            <br />
            <div class="row slideanim">
                <div class="col-sm-4">
                    <img src="/assets/svg/icon PHP Hosting_PHP Semua Versi.svg" />

                    <h4>
                        <b>PHP Semua Versi</b>
                    </h4>
                    <p>
                        Pilih mulai versi 5.3 s/d PHP 7.
                        <br />Ubah sesuka anda!
                    </p>
                </div>
                <div class="col-sm-4">
                    <img src="/assets/svg/icon PHP Hosting_My SQL.svg" />
                    <h4>
                        <b>Mysql Versi 5.6</b>
                    </h4>
                    <p>
                        Nikmati Mysql versi terbaru, tercepat dan
                        <br />kaya akan fitur.
                    </p>
                </div>
                <div class="col-sm-4">
                    <img src="/assets/svg/icon PHP Hosting_CPanel.svg" />
                    <h4>
                        <b>Panel Hosting cPanel</b>
                    </h4>
                    <p>
                        Kelola website dengan panel canggih yang
                        <br />familiar di hati anda.
                    </p>
                </div>
            </div>
            <br />
            <br />
            <div class="row slideanim">
                <div class="col-sm-4">
                    <img src="/assets/svg/icon PHP Hosting_garansi uptime.svg" />
                    <h4>
                        <b>Garansi Uptime 99.9%</b>
                    </h4>
                    <p>
                        Data center yang mendukung kelangsungan
                        <br />website Anda 24/7.
                    </p>
                </div>
                <div class="col-sm-4">
                    <img src="/assets/svg/icon PHP Hosting_InnoDB.svg" />
                    <h4>
                        <b>Database InnoDB Unlimited</b>
                    </h4>
                    <p>
                        Jumlah dan ukuran database yang tumbuh
                        <br />sesuai dengan kebutuhan Anda.
                    </p>
                </div>
                <div class="col-sm-4">
                    <img src="/assets/svg/icon PHP Hosting_My SQL remote.svg" />
                    <h4>
                        <b>Wildcard Remote MySQL</b>
                    </h4>
                    <p>
                        Mendukung s/d 25 max_user_connections
                        <br />dan 100 max_connection.
                    </p>
                </div>
            </div>

            <hr />
            <br />
        </div>
    </section>


    <section class="framework">
        <div class="container">
            <div class="row">
                <div class="text-center">
                    <h3>Mendukung Penuh Framework Laravel</h3>
                </div>
                <br />
                <div class="col-sm-6 col-md-6">
                    <p>
                        Tak perlu menggunakan dedicated server ataupun VPS
                        <br />yang mahal. Layanan PHP hosting murah kami
                        <br />mendukung penuh framework favorit Anda
                    </p>
                    <div class="text-left">
                        <table class="table-responsive">
                            <tbody>
                                <tr>
                                    <td class="checklist">
                                        <i class="fas fa-check-circle"></i>
                                    </td>
                                    <td>
                                        Install Laravel
                                        <strong>1 klik</strong> dengan softacolous Installer.
                                    </td>
                                </tr>

                                <tr class="success">
                                    <td class="checklist">
                                        <i class="fas fa-check-circle"></i>
                                    </td>
                                    <td>
                                        Mendukung exstensi
                                        <strong>PHP MCrypt, phar, mbstring, json,</strong> dan
                                        <strong>fileinfo.</strong>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="checklist">
                                        <i class="fas fa-check-circle"></i>
                                    </td>
                                    <td>
                                        Tersedia
                                        <strong>Composer</strong> dan
                                        <strong>SSH</strong> untuk menginstall packages piihan Anda.
                                    </td>
                                </tr>

                                <tr class="success">
                                    <td>
                                        <h6>Nb.</h6>
                                    </td>
                                    <td>
                                        <h6>Composer dan SSH hanya tersedia pada paket Personal dan Bisnis</h6>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="col-sm-6 col-md-6 text-center">
                            <button type="button" class="btn btn-primary btn-lg">
                                <b>Piih Hosting Anda</b>
                            </button>
                        </div>
                    </div>
                </div>
                <br />
                <div class="col-sm-6 col-md-6">
                    <img src="/assets/svg/illustration banner support laravel hosting.svg" />
                </div>
            </div>
        </div>
    </section>


    <section class="modul">
        <div class="container text-center slideanim">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h3>Modul lengkap untuk menjalankan Aplikasi PHP Anda.</h3>
                    <br />

                    <?php
                    $json = file_get_contents('assets/json/modul.json');
                    $folders = json_decode($json);
                    foreach ($folders as $key => $value) {
                        echo '
                        <div class="col-md-3 col-sm-6 col-xs-6 col-xs-12">
                        <table class="table table-sm">
                          <tbody>';
                        foreach ($value->list as $key => $list) {
                            echo '<tr>
                            <td align="left">' . $list . '</td>
                          </tr>';
                        }
                        echo '
                          </tbody>
                        </table>
                      </div>
                        ';
                    }
                    ?>
                </div>
            </div>
            <br />

            <a href="#" class="btn btn-default">
                <b>Selengkapnya</b>
            </a>
        </div>
    </section>

    <section class="linux">
        <div class="container">
            <div class="col-sm-6 col-md-12">
                <div class="row">
                    <div class="text-left">
                        <h2>
                            Linux Hosting yang Stabil
                            <br />dengan Teknologi LVE
                        </h2>
                    </div>
                    <br />

                    <div class="col-sm-3 col-md-6" style="margin-left: -10px;">
                        <p align="left">
                            SuperMicro
                            <b>Intel Xeon 24-Cores</b> server dengan RAM
                            <b>128 GB</b> dan
                            <br />teknologi
                            <b>LVE CloudLinux</b> untuk stabilitas server Anda. Dilengkapi
                            <br />dengan
                            <b>SSD</b> untuk kecepatan
                            <b>MySQL</b> dan caching. Apache load balancer
                            <br />berbasis LiteSpeed Technologies,
                            <b>CageFS</b> security,
                            <b>Raid-10</b> proctection
                            <br />dan auto backup untuk keamanan website PHP Anda.
                        </p>

                        <div class="text-left">
                            <div class="col-sm-6 col-md-6 text-center">
                                <button type="button" class="btn btn-primary btn-lg">
                                    <b>Pilih Hosting Anda</b>
                                </button>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="col-sm-9 col-md-6 slideanim">
                        <img src="/assets/images/Image support.png" />
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="share">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h5>Bagikan jika Anda menyukai halaman ini.</h5>
                </div>
                <div class="col-md-6 col-sm-6 text-center">
                    <div class="socmed-shares">
                        <div class="socmed-share socmed-share-facebook">
                            <a target="_blank" href class="socmed-share-link">
                                <i class="fab fa-facebook-f socmed-share-logo"></i>
                            </a>
                            <div class="socmed-share-count-box">
                                <span class="socmed-share-count">80K</span>
                            </div>
                        </div>
                        <div class="socmed-share socmed-share-twitter">
                            <a target="_blank" href class="socmed-share-link">
                                <i class="fab fa-twitter socmed-share-logo"></i>
                            </a>
                            <div class="socmed-share-count-box">
                                <span class="socmed-share-count">450</span>
                            </div>
                        </div>
                        <div class="socmed-share socmed-share-googleplus">
                            <a target="_blank" href class="socmed-share-link">
                                <i class="fab fa-google-plus-g socmed-share-logo"></i>
                            </a>
                            <div class="socmed-share-count-box">
                                <span class="socmed-share-count">1900</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bantuan">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-9">
                    <span>
                        Perlu
                        <span>
                            <b>BANTUAN</b>
                        </span> ? Hubungi Kami :
                        <strong>0274-5305505</strong>
                    </span>
                </div>
                <div class="col-12 col-sm-3">
                    <div class="float-left text-right">
                        <button type="button" class="btn btn-default btn-lg">
                            <img src="/assets/images/chat.png" />
                            <strong>Live Chat</strong>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="footer-menu">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-3 mt-5 mt-lg-0">
                    <h5>HUBUNGI KAMI</h5>
                    <a href="tel:+62745305505">0274-5305505</a>
                    <p>
                        Senin - Minggu
                        <br />24 Jam Non Stop
                    </p>
                    <p>
                        Jl. Selokan Mataram Monjali
                        <br />Karangjati MT I/304
                        <br />RT 019 RW 042
                        <br />Sinduadi, Mlati, Sleman
                        <br />Yogyakarta 55284
                    </p>
                </div>

                <div class="col-12 col-sm-6 col-lg-3 mt-5 mt-lg-0">
                    <h5>LAYANAN</h5>
                    <ul class="nav navbar-link">
                        <a href="#">
                            <li>Domain</li>
                        </a>
                        <a href="#">
                            <li>Shared Hosting</li>
                        </a>
                        <a href="#">
                            <li>Cloud VPS Hosting</li>
                        </a>
                        <a href="#">
                            <li>Managed VPS Hosting</li>
                        </a>
                        <a href="#">
                            <li>Web Builder</li>
                        </a>
                        <a href="#">
                            <li>Keamanan SSL/HTTPS</li>
                        </a>
                        <a href="#">
                            <li>Jasa Pembuatan Website</li>
                        </a>
                        <a href="#">
                            <li>Program Affiliasi</li>
                        </a>
                    </ul>
                </div>

                <div class="col-12 col-sm-6 col-lg-3 mt-5 mt-lg-0">
                    <h5>SERVICE HOSTING</h5>
                    <ul class="nav navbar-link">
                        <a href="#">
                            <li>Hosting Murah</li>
                        </a>
                        <a href="#">
                            <li>Hosting Indonesia</li>
                        </a>
                        <a href="#">
                            <li>Hosting Singapura SG</li>
                        </a>
                        <a href="#">
                            <li>Hosting PHP</li>
                        </a>
                        <a href="#">
                            <li>Hosting Wordpress</li>
                        </a>
                        <a href="#">
                            <li>Hosting Laravel</li>
                        </a>
                    </ul>
                </div>

                <div class="col-12 col-sm-6 col-lg-3 mt-5 mt-lg-0">
                    <h5>TUTORIAL</h5>
                    <ul class="nav navbar-link">
                        <a href="#">
                            <li>knowledgebase</li>
                        </a>
                        <a href="#">
                            <li>Blog</li>
                        </a>
                        <a href="#">
                            <li>Cara Pembayaran</li>
                        </a>
                    </ul>
                </div>

            </div>
            <div class="row">

                <div class="col-xs-12 col-sm-6 col-lg-3 mt-5 mt-lg-0">
                    <h5>TENTANG KAMI</h5>
                    <ul class="nav navbar-link">
                        <a href="#">
                            <li>Tim Niagahoster</li>
                        </a>
                        <a href="#">
                            <li>Karir</li>
                        </a>
                        <a href="#">
                            <li>Events</li>
                        </a>
                        <a href="#">
                            <li>Penawaran & Promo Spesial</li>
                        </a>
                        <a href="#">
                            <li>Kontak Kami</li>
                        </a>
                    </ul>
                </div>

                <div class="col-xs-12 col-sm-6 col-lg-3 mt-5 mt-lg-0">
                    <h5>KENAPA PILIH NIAGAHOSTER?</h5>
                    <ul class="nav navbar-link">
                        <a href="#">
                            <li>Support Terbaik</li>
                        </a>
                        <a href="#">
                            <li>Garansi Harga Termurah</li>
                        </a>
                        <a href="#">
                            <li>Domain Gratis Selamanya</li>
                        </a>
                        <a href="#">
                            <li>Datacenter Hosting Terbaik</li>
                        </a>
                        <a href="#">
                            <li>Review Pelanggan</li>
                        </a>
                    </ul>
                </div>

                <div class="col-xs-12 col-sm-6 col-lg-3 mt-5 mt-lg-0">
                    <h5>NEWSLETTER</h5>
                    <form class="pull-left">
                        <div class="input-group">
                            <span class="input-groupx">
                                <input class="btn btn-lgx" name="email" type="email" placeholder="Email" required />
                                <button class="btnx btn-info btn-lgx" type="submit">Berlangganan</button>
                            </span>
                        </div>
                        <ul class="nav navbar-link">
                            <li style="color: #8d8d8d !important;">Dapatkan promo dan konten menarik
                                <br />dari penyedia Hosting Anda.</li><br />
                        </ul>
                    </form>
                </div>

                <div class="col-xs-12 col-sm-6 col-lg-3 mt-5 mt-lg-0">
                    <div style="padding-top: 65px;">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-4 col-xxs-4 text-center">
                                <a href="#" target="_blank">
                                    <div class="button-wrap">
                                        <div class="button-inner-wrap">
                                            <i class="fab fa-facebook-f inactive"></i>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-4 col-xxs-4 text-center">
                                <a href="#" target="_blank">
                                    <div class="button-wrap">
                                        <div class="button-inner-wrap">
                                            <i class="fab fa-twitter inactive"></i>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-4 col-xxs-4 text-center">
                                <a href="#" target="_blank">
                                    <div class="button-wrap">
                                        <div class="button-inner-wrap">
                                            <i class="fab fa-google-plus-g inactive"></i>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-xs-12">
                    <h5>PEMBAYARAN</h5>
                    <div class="col-md-1 col-sm-6 col-xs-4 col-xxs-4">
                        <a href="#">
                            <img src="/assets/svg/logo-bca-bordered.svg" />
                        </a>
                    </div>
                    <div class="col-md-1 col-sm-6 col-xs-4 col-xxs-4">
                        <a href="#">
                            <img src="/assets/svg/mandiriclickpay.svg" />
                        </a>
                    </div>
                    <div class="col-md-1 col-sm-6 col-xs-4 col-xxs-4">
                        <a href="#">
                            <img src="/assets/svg/logo-bni-bordered.svg" />
                        </a>
                    </div>
                    <div class="col-md-1 col-sm-6 col-xs-4 col-xxs-4">
                        <a href="#">
                            <img src="/assets/svg/Visa.svg" />
                        </a>
                    </div>
                    <div class="col-md-1 col-sm-6 col-xs-4 col-xxs-4">
                        <a href="#">
                            <img src="/assets/svg/Mastercard.svg" />
                        </a>
                    </div>
                    <div class="col-md-1 col-sm-6 col-xs-4 col-xxs-4">
                        <a href="#">
                            <img src="/assets/svg/atmbersama.svg" />
                        </a>
                    </div>
                    <div class="col-md-1 col-sm-6 col-xs-4 col-xxs-4">
                        <a href="#">
                            <img src="/assets/svg/permatabank.svg" />
                        </a>
                    </div>
                    <div class="col-md-1 col-sm-6 col-xs-4 col-xxs-4">
                        <a href="#">
                            <img src="/assets/svg/prima.svg" />
                        </a>
                    </div>
                    <div class="col-md-1 col-sm-6 col-xs-4 col-xxs-4">
                        <a href="#">
                            <img src="/assets/svg/Alto.svg" />
                        </a>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 col-xs-12">
                    <h6>Aktivasi instan dengan e-Payment. Hosting dan domain langsung aktif!</h6>
                </div>
            </div>
        </div>
    </section>

    <section class="copyright">
        <div class="container">
            <div class="row mt-5">
                <hr />
                <div class="col-12 col-md-7 text-left">
                    Copyright ©2016 Niagahoster | Hosting powered by PHP7,
                    CloudLinux, CloudFlare, BitNinja and DC Biznet Technovillage Jakarta
                    Cloud VPS Murah powered by Webuzo
                    Softaculous, Intel SSD and cloud computing technology <br />
                </div>
                <br />
                <div class="col-12 col-md-4 mt-4 mt-md-0 text-right martop">
                    <span>
                        <a href="#">Syarat dan Ketentuan</a> |
                    </span>
                    <span>
                        <a href="#">Kebijakan Privasi</a>
                    </span>
                </div>
            </div>
        </div>
    </section>

    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function() {
            $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
                if (this.hash !== "") {
                    event.preventDefault();

                    var hash = this.hash;

                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 900, function() {
                        window.location.hash = hash;
                    });
                }
            });

            $(window).scroll(function() {
                $(".slideanim").each(function() {
                    var pos = $(this).offset().top;

                    var winTop = $(window).scrollTop();
                    if (pos < winTop + 600) {
                        $(this).addClass("slide");
                    }
                });
            });
        })
    </script>
</body>

</html>